class nginx::config
    (
    $prefix="/etc/nginx", 
    $conf
    )
    {
    # should be created by nginx package.
    file
        {
        "$prefix":
        ensure => directory,
        owner => 'root', 
        group => 'root', 
        mode => '0644',
        }
    
    file
        {
        "$prefix/nginx.conf":
        ensure => present, 
        owner => 'root', 
        group => 'root', 
        mode => '0644',
        content => template('nginx/nginx.conf.erb'),
        }

    class{'nginx::config::auth_basic_user':}
    contain 'nginx::config::auth_basic_user' 
    }

class nginx::config::auth_basic_user(Hash $files={}, $dir="/etc/nginx")
    {
    # Wraps up creation of http_basic_auth_file into a class for use with hiera.
    # $files should be a hash of http_basic_auth_file resource data.
    # $dir is the directory in which the files are written.  Default is the nginx configuration
    # directory.

    create_resources(nginx::config::auth_basic_user_file, $files, {dir=>"$dir"})
    }


# Represents an nginx auth_basic_user_file.
# The resource title is the name of the file; the file itself will be created in 
# $dir.
# $ensure: present | absent.
# $entries : a hash of name: password hash items. You create
# password hashes yourself as suggested in the nginx documentation for auth_basic_user_file.
# The parameter value can be omitted if $ensure = absent.

define nginx::config::auth_basic_user_file
    (
    Enum['present', 'absent'] $ensure=present,
    Hash $entries={},
    $dir,
    )
    {    
    file
        {
        "$dir/$title":
        ensure => $ensure, 
        owner => 'root', 
        group => 'root',
        mode => '0644',
        content => template('nginx/auth_basic_user_file.erb')
        }
    }

