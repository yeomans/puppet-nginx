class nginx::repo
    {
    $distro = downcase($::operatingsystem)

    apt::pin
        {
        'nginx.org':
        originator => 'nginx.org',
        priority   => 500,
        }->
    apt::source 
        {
        'nginx.org':
          location => "http://nginx.org/packages/${distro}",
          release => "${::lsbdistcodename}",
          repos => "nginx",
          key => {id => '573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62', 'source' => 'http://nginx.org/keys/nginx_signing.key'},
        }
    }
