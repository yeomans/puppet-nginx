#Puppet-Nginx

Puppet-Nginx is a Puppet module for management of nginx.  The idea is to be able to write
a configuration entirely in hiera.

Since configuration tinkering should be done in Puppet, the structure of the generated files 
is arranged to work with Puppet.



## Sample hiera configuration

```yaml
nginx::config::user: "nginx"
nginx::config::group: "nginx"
nginx::config::pid: "var/run/nginx.pid"
    
nginx::config::http::keepalive_timeout: 65
nginx::config::http::map:
    '$request:$status $is_upload':
        '~^PUT:2': 1
        'default': 0
nginx::config::http::servers:
    www.example.com:
        ensure: 'present'
        listen: "192.168.1.1:443 ssl"
        server_name: "www.example.com"
        add_header: ['Strict-Transport-Security max-age=31536000']
        ssl_certificate: '/etc/ssl/localcerts/www.example.com.crt'
        ssl_certificate_key: '/etc/ssl/private/www.example.com.key'
        ssl_trusted_certificate: '/etc/ssl/localcertswww.example.com.crt'
        access_log: ['/var/log/nginx/www.example.com/access.log']
        error_log: '/var/log/nginx/www.example.com/error.log'
        locations:
            '/':
            root: '/usr/local/share/nginx'
            proxy_pass: "http://localhost:81"
            proxy_set_header: 
                Host: "$host"
                X-Real-IP: "$remote_addr"
nginx::config::http::upstreams:
    upstream_appserver:
        name: 'appserver'
        server: ["%{hiera('dbfweb_app::ip_address')}"]                
                
nginx::config::auth_basic_user::files::
    "www.example.com.htpasswd":
        entries:
            ray: "$apr1$l2Gj5e3o$QJ/kkvXnW9Gl36dONPJss."
            tom: "$apr1$ocEIGthx$dmNrCsDBz8xU1RBHj8t5F0"
```